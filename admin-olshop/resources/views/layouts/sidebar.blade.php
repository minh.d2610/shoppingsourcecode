<div class="user-panel">
      </div>
      <!-- /.search form -->
      <!-- sidebar menu: : style can be found in sidebar.less -->
      
      
        <ul class="sidebar-menu" data-widget="tree">
            <li class="header">MAIN NAVIGATION</li>
            
            <li class="treeview">
              <a href="#">
                <i class="fa fa-dashboard"></i> <span>Quản lý sản phẩm</span>
                <span class="pull-right-container">
                  <i class="fa fa-angle-left pull-right"></i>
                </span>
              </a>
              <ul class="treeview-menu">
                <li><a href="{{ url('barang') }}"><i class="fa fa-circle-o"></i>Danh sách sản phẩm</a>
                </li>
                <li><a href="{{ url('barang/habis') }}"><i class="fa fa-circle-o"></i>Danh sách sản phẩm đã bán hết</a>
                </li>
                <li><a href="{{ url('barang/create') }}"><i class="fa fa-circle-o"></i>Thêm sản phẩm</a>
                </li>
                </li>
              </ul>
            </li>

            <li>
              <a href="{{ url('kategori') }}">
                <i class="fa fa-amazon"></i> <span>Quản lý loại sản phẩm</span>
              </a>
            </li>

            <li>
              <a href="{{ url('konfirmasi') }}">
                <i class="fa fa-opera"></i> <span>Đơn hàng đợi xác nhận</span>
              </a>
            </li>

            <li>
              <a href="{{ url('pesanan') }}">
                <i class="fa fa-adjust"></i> <span>Đơn đã hoàn thành</span>
              </a>
            </li>

            <li>
              <a href="{{ url('keluar') }}">
                <i class="fa fa-adjust"></i> <span>Đăng xuất</span>
              </a>
            </li>
            
          </ul>