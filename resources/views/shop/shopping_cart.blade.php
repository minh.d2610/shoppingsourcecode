@extends('layouts.master')

@section('content')

<div class="span9">
    <ul class="breadcrumb">
		<li><a href="index.html">Home</a> <span class="divider">/</span></li>
		<li class="active">Giỏ hàng</li>
    </ul>
	<h3>Giỏ hàng [<small>{{count(Cart::content()) }} sản phẩm</small>]<a href="{{ url('shopping-cart/destroy') }}" class="btn btn-large pull-right">Xóa toàn bộ</a></h3>	
	<hr class="soft"/>
			
	<table class="table table-bordered">
              <thead>
                <tr>
                  <th>Sản phẩm</th>
                  <th>Số lượng</th>
				  <th>Đơn giá</th>
                  <th>Số tiền</th>
				</tr>
              </thead>
              <tbody>
              	@foreach($barangs as $barang)
                <tr>
                  <td>{{ $barang->name }}</td>
				  <td>
					<div class="input-append">
						<button rowId="{{ $barang->rowId }}" class="btn kurangi-qty" type="button"><i class="icon-minus"></i></button>
						<input disabled="" class="span1" style="max-width:34px" placeholder="{{ $barang->qty }}" id="appendedInputButtons" size="16" type="text">
						<a href="{{ $barang->rowId }}" class="btn add-qty"><i class="icon-plus"></i></a>				
					</div>
				  </td>
                  <td>{{ number_format($barang->price, 0)}}đ</td>
                  <td>{{ number_format($barang->subtotal, 0)}}đ</td>
                </tr>
                @endforeach
                <tr>
                	<th class="text-center" colspan="3">Tổng tiền hàng</th>
                	<td class="label label-important">{{ Cart::total() }}đ</td>
                </tr>
				</tbody>
            </table>
			</table>

	<a href="{{ url('/') }}" class="btn btn-large"><i class="icon-arrow-left"></i>Tiếp tục mua</a>
	@if(count(Cart::content()) != 0)
	<a href="{{ url('shopping-cart/checkout') }}" class="btn btn-large pull-right">Thanh toán<i class="icon-arrow-right"></i></a>
	@endif
	
</div>

@endsection

@section('scripts')

<script type="text/javascript">
	$(document).ready(function(){
		$('.add-qty').click(function(e){
			e.preventDefault();
			var rowId = $(this).attr('href');
			window.location.href = "{{ url('shopping-cart/update') }}"+'/'+rowId;
		});

		$('.kurangi-qty').click(function(e){
			e.preventDefault();
			var rowId = $(this).attr('rowId');
			window.location.href = "{{ url('shopping-cart/kurangi') }}"+'/'+rowId;
		});
	});
</script>

@endsection

@section('scripts')

<script>
		$(document).ready(function(){
			var flash = "{{ Session::has('pesan') }}";
			if(flash){
				var pesan = "{{ Session::get('pesan') }}";
				swal('success', pesan, 'success');
			}
		});
	</script>

@endsection