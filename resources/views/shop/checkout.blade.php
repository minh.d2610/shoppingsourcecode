@extends('layouts.master')

@section('content')

<div class="span9">
    <ul class="breadcrumb">
		<li><a href="index.html">Home</a> <span class="divider">/</span></li>
		<li class="active">Registration</li>
    </ul>
	<h3> Checkout</h3>	
	<div class="well">
	<!--
	<div class="alert alert-info fade in">
		<button type="button" class="close" data-dismiss="alert">×</button>
		<strong>Lorem Ipsum is simply dummy</strong> text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s
	 </div>
	<div class="alert fade in">
		<button type="button" class="close" data-dismiss="alert">×</button>
		<strong>Lorem Ipsum is simply dummy</strong> text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s
	 </div>
	 <div class="alert alert-block alert-error fade in">
		<button type="button" class="close" data-dismiss="alert">×</button>
		<strong>Lorem Ipsum is simply</strong> dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s
	 </div> -->
	<form id = "checkout" class="form-horizontal" action="{{ url('shopping-cart/bayar') }}" method="POST">
		{{ csrf_field() }}

		<div class="control-group">
			<label class="control-label" for="inputFname">Tên người nhận <sup>*</sup></label>
			<div class="controls">
			  <input type="text" name="nama_penerima" id="inputFname" placeholder="Nguyễn Văn A" value="{{ old('nama_penerima') }}" autofocus required>
			  <!-- @if ($errors->has('nama_penerima'))
	                    <span class="help-block">
	                        <strong>{{ $errors->first('nama_penerima') }}</strong>
	                    </span>
	                @endif -->
			</div>
		</div>

		<div class="control-group">
			<label class="control-label" for="phone">Số điện thoại <sup>*</sup></label>
			<div class="controls">
			  <input type="text" name="phoneNumber" id="phone" placeholder="012345678" autofocus required>
			  <!-- @if ($errors->has('phoneNumber'))
	                    <span class="help-block">
	                        <strong>{{ $errors->first('phoneNumber') }}</strong>
	                    </span>
	                @endif -->
			</div>
		</div>

		<div class="control-group">
			<label class="control-label" for="aditionalInfo">Địa chỉ <sup>*</sup></label>
			<div class="controls">
			  <textarea name="alamat" id="aditionalInfo" cols="26" rows="5" autofocus required></textarea>
			  <!-- @if ($errors->has('alamat'))
	                    <span class="help-block">
	                        <strong>{{ $errors->first('alamat') }}</strong>
	                    </span>
	                @endif -->
			</div>
		</div>
	
	<div class="control-group">
			<div class="controls">
				<input type="hidden" name="email_create" value="1">
				<input type="hidden" name="is_new_customer" value="1">
				<input class="btn btn-large btn-success" type="submit" value="Bayar">
			</div>
		</div>		
	</form>
</div>

</div>
<script>
	$("#checkout").submit(function (evt) {

//At this point the browser will have performed default validation methods.

//If you want to perform custom validation do it here.
if ($(".phoneNumber").val().length === 0) {
	($(".phoneNumber").
  	evt.preventDefault();
  	return;
}
alert("Values are correct!");

});
</script>
@endsection