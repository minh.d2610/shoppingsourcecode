@extends('layouts.master')

@section('content')

<table class="table table-bordered">
	<thead>
		<tr>
			<th class="text-center" colspan="4">Hóa đơn</th>
		</tr>
	</thead>
	<tbody>
		<tr>
			<th>Tên sản phẩm</th>
			<th>Số lượng</th>
			<th>Thành tiền</th>
		</tr>
		@foreach($barangs as $index=>$barang)
		<tr>
			<td>{{ $barang->name }}</td>
			<td>{{ $barang->qty }}</td>
			<td>{{ $barang->subtotal() }}đ</td>
		</tr>
		@endforeach
		<tr>
			<th colspan="2">Tổng tiền</th>
			<th style="background: lime;">{{ $total }}đ</th>
		</tr>
		<!-- <tr>
			<th colspan="1">Transfer Ke :</th>
			<td colspan="2">BRI : Atas Nama <b><i>Fulan Bin Fulan</i></b><br>No. Rekening <b><i>123456789</i></b></td>
		</tr> -->
	</tbody>
</table>

@endsection

@section('scripts')

<script>
		$(document).ready(function(){
			var flash = "{{ Session::has('pesan') }}";
			if(flash){
				var pesan = "{{ Session::get('pesan') }}";
				swal('success', pesan, 'success');
			}
		});
	</script>

@endsection